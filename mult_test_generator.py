"""Used to generate a few test cases for GF mult using pyfinite."""

from pyfinite import ffield
import random

gf8 = ffield.FField(8)

for idx in range(5):
	x = random.randint(0, 256)
	y = random.randint(0, 256)
	result = gf8.Multiply(x, y)
	print(x, y, result)
