from __future__ import print_function
from __future__ import division


class GaloisField(object):
  """Helper functions on GaloisField(2^X)."""
  def __init__(self, dimension):
    super(GaloisField, self).__init__()
    self.dimension = dimension
    self.polynomial = 0
    for idx in self.GetPolynomial():
      self.polynomial |= 1 << idx

  def GetPolynomial(self):
    """Return a list of indices in which the polynomial is 1.

    Args:
      dimension: Integer. Look for a poly in GF(2^dimension).

    Raises:
      ValueError: if a suitable polynomial is not found.
    """
    if self.dimension == 128:
      return [128, 7, 2, 1, 0]
      # return [128, 7, 3, 1, 0]
    if self.dimension == 8:
      return [8, 4, 3, 2, 0]

    raise ValueError('Unsupported dimension for polynomial generation.')

  def Multiply(self, x, y, style='traditional'):
    """Multiply two numbers in GF(2^dimension).

    Args:
      x: argument 1
      y: argument 2
      style: string. One of 'traditional', 'reduce_remainders'

    Returns:
      x * y in GF(2^self.dimension)
    """
    if style == 'traditional':
      return self._TraditionalMultiply(x, y)
    if style == 'reduce_remainders':
      return self._ReduceRemaindersMultiply(x, y)

  def _TraditionalMultiply(self, x, y):
    """Traditional (long form) multiply two numbers in GF(2^dimension).

    Take each bit of x (LSB to MSB), if 1, then xor a left shifted version of y
    with result (just like) in traditional multiplication. if y's MSB after
    shifting is 1 (which will imply the next result is going to overflow)
    divide it by the poly till it becomes < 2^x. In this case it is guaranteed
    to come back in 1 substraction.

    Args:
      x: argument 1
      y: argument 2

    Returns:
      x * y in GF(2^self.dimension)
    """
    result = 0
    for idx in range(self.dimension):
      if x & 1:
        result ^= y
      # print('idx = {}, y = 0x{:x}'.format(idx, y))
      y <<= 1
      # print('y << 1 = 0x{:x}'.format(y))
      if y & 1 << self.dimension:
        y ^= self.polynomial
      # print('y ^ poly = 0x{:x}'.format(y))
      x >>= 1
    return result

  def _ReduceRemaindersMultiply(self, x, y):
    """Multiply in a non finite field and use remainders to close.

    An alternative way to multiply two numbers in GF:
    1) get a "product" by doing long form multiply, but with xors instead of
       add. This will give us a 2^self.dimension-1 polynomial.
    2) For every bit that is 1 in the higher dimensions, reduce the product by
       XOR-ing (folding in) the remainder of 2^x/prime poly.

    Computing the remainders: see _Remainders()
      In hardware, these numbers may be computed once and stored/hardcorded.


    Args:
      x: argument 1
      y: argument 2

    Returns:
      x * y in GF(2^self.dimension)
    """
    # step one - traditional multiply.

    def _Remainders():
      """TODO(bytefifo): Do this once and reuse!"""
      remainders = [0] * (2*self.dimension-1)
      remainders[0] = 1
      for idx in range(1, 2*self.dimension-1):
        if remainders[idx-1] & 1 << self.dimension - 1:
          remainders[idx] = (remainders[idx-1] << 1) ^ self.polynomial
        else:
          remainders[idx] = (remainders[idx-1] << 1)
      return remainders

    # step 1: long form multiply, with XORs.
    result = 0
    for idx in range(self.dimension):
      if x & 1:
        result ^= y << idx
      x >>= 1
    # print('Shifted XOR tree result = 0x{:x}'.format(result))

    # step 2: reduce the higher dimensions
    remainders = _Remainders()
    # print(['{:x}'.format(x) for x in remainders])
    for idx in range(self.dimension, (2 * self.dimension) - 1):
      if result & 1 << idx:
        result ^= remainders[idx]
        # print('idx = {}, remainder = {:x}, result = {:x}'.format(idx, remainders[idx], result))

    # step 3, discard the msbs.
    result = result % 2**self.dimension

    return result
