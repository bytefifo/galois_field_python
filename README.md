** Galois field multiplier - implementations in Python **

Our goal is to implement a performant GF multiplier in SystemVerilog. This
repository contains python implementations of select ways of performing some
operations in a Galois field. This helps validate and debug the SystemVerilog
implementation.

## Testing

The GF multiplier in pyfinite (ffield.FField()) does not support multplies at
fields above GF(100). The corresponding ploys are not present. Hence we've used
GF(8) to validate the functions. This leads to a small coverage hole, but these
are math units and hopefully scale correctly.

## Required setup

The code works with Python 2.6+. The build flow uses bazel. To test your setup,

`bazel test :galois_field_test`

Should return PASSED.

### Python packages

For testing

*		pytest
* 	parameterized