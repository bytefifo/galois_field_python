"""Tests for :galois_field."""

import unittest
from parameterized import parameterized

from galois_field_python import galois_field

class GaloisFieldTest(unittest.TestCase):
  """Unit tests for GaloisField."""

  def setUp(self):
    self.GF8 = galois_field.GaloisField(8)
    self.GF128 = galois_field.GaloisField(128)

  def testPolySanity(self):
    """Check that Poly's have MSB and LSB set."""
    poly = self.GF8.GetPolynomial()
    self.assertIn(self.GF8.dimension, poly)
    self.assertIn(0, poly)

  @parameterized.expand(['traditional', 'reduce_remainders'])
  def testMultiply(self, style):
    """GF Mult gets expected result."""
    # basics
    self.assertEquals(self.GF8.Multiply(0, 0, style=style), 0)
    self.assertEquals(self.GF8.Multiply(0, 1, style=style), 0)
    self.assertEquals(self.GF8.Multiply(1, 1, style=style), 1)
    self.assertEquals(self.GF8.Multiply(0xff, 1, style=style), 0xff)

    # Expected values derived from pyfinite.ffield."""
    self.assertEquals(self.GF8.Multiply(98, 238, style=style), 72)
    self.assertEquals(self.GF8.Multiply(83, 57, style=style), 26)
    self.assertEquals(self.GF8.Multiply(191, 164, style=style), 93)
    self.assertEquals(self.GF8.Multiply(151, 247, style=style), 34)
    self.assertEquals(self.GF8.Multiply(210, 72, style=style), 96)
    self.assertEquals(self.GF8.Multiply(57, 6, style=style), 150)
    self.assertEquals(self.GF8.Multiply(148, 29, style=style), 159)
    self.assertEquals(self.GF8.Multiply(199, 84, style=style), 64)
    self.assertEquals(self.GF8.Multiply(33, 149, style=style), 194)
    self.assertEquals(self.GF8.Multiply(191, 29, style=style), 215)

  # def testDebug(self):
  #   """Will not fail, just debug prints."""
  #   self.assertEquals(self.GF8.Multiply(98, 238, style='reduce_remainders'), 72)

  @parameterized.expand([(0x00, [0x1, 0x2, 0x3, 0x4]),
                         (0x00, [0x0, 0x0, 0x0, 0x0]), 
                         (0x3d, [0x4f, 0xce, 0x73, 0x43]),
                         (0xff, [0xff, 0x00, 0xff, 0xff])])
  def testGMACPipelineEquivalence(self, starting_X, messages):
    """Make sure that the following formula is correct."""
    H = 0x27  # A random number
    # GMAC Xi = (Xi-1 xor Mi) . H
    Multiply = self.GF8.Multiply
    def _gmac(previous, message):
      return Multiply((previous ^ message), H, style='reduce_remainders')

    X = starting_X
    for message in messages:
      X = _gmac(X, message)

    H_powers = [H]
    for idx in range(1, 4):
      H_powers.append(Multiply(H_powers[idx-1], H))

    X_parallel = (Multiply(H_powers[0], messages[3]) ^
                  Multiply(H_powers[1], messages[2]) ^
                  Multiply(H_powers[2], messages[1]) ^
                  Multiply(H_powers[3], (messages[0] ^ starting_X)))
    self.assertEqual(X, X_parallel)

  def testGMACVerilogTestInputs(self):
    """Compute/test expected result in the verilog testcase."""
    message = [0] * 4
    message[0] = 0x00000000000000000000000000000000
    message[1] = 0x00c0000000000e000003000000000001
    message[2] = 0x10000000000a00000006000000050002
    message[3] = 0x8000b000000000000007000000040003
    H = 0xabcdef01234567890123456789abcdef
    starting_X = 0x0

    Multiply = self.GF128.Multiply
    def _gmac(previous, message):
      return Multiply((previous ^ message), H, style='reduce_remainders')

    X = starting_X
    for m in message:
      X = _gmac(X, m)
    # print('0x{:x}'.format(X))

  def testIntermediatePrints(self):

    def _bit_swizzle(number):
      out = 0
      for idx in range(127):
        out |= (number >> idx) & 1
        out <<= 1
      out |= (number >> 128) & 1
      return out

    message = 0;
    H = 0x66e94bd4ef8a2c3b884cfa59ca342b2e
    H_little_endian = 0x2e2b34ca59fa4c883b2c8aefd44be966

    Multiply = self.GF128.Multiply
    def _gmac(previous, message):
      return Multiply((previous ^ _bit_swizzle(message)), _bit_swizzle(H), style='reduce_remainders')

    # gmac = _gmac(0x0, 0x2)
    # print('0x{:x}'.format(gmac))
    e_k_0 = 0x58e2fccefa7e3061367f1d57a4e7455a
    e_k_1 = 0x0388dace60b6a392f328c2b971b2fe78
    # e_k_1 = 0x78feb271b9c228f392a3b660ceda8803
    gmac = _gmac(0x0, e_k_1)
    print('0x{:x}'.format(_bit_swizzle(gmac)))
    gmac = _gmac(gmac, 0x80)
    print('0x{:x}'.format(_bit_swizzle(gmac)))    
    print('0x{:x}'.format(_bit_swizzle(gmac ^ _bit_swizzle(e_k_0))))    

if __name__ == "__main__":
  unittest.main()